# Survey MicroService

App is created for users how wants to take part in surveys. It also calculate and display the estimated probability of 
the candidate meeting the criteria for becoming a panelist.

Documentation for the endpoints can be accessed via http://127.0.0.1:5000/api/v1/docs

## How the probability is calculated
The probability is calculated using following equations:

```
if user answered all the questions correctly:
    probability = 1
elif user provided any wrong answer:
    probability = 0

skip all the questions which user already answered.

probability_of_correct_answer = (correct_answers + 1)/(all_answers + 1)
probability_of_wrong_answer = (correct_answers)/(all_answers + 1)

probability = probability_of_correct_answer * probability_of_wrong_answer / 2
```

Thanks of this equation we can achieve following rules:
 - if no answer was provided before in the survey, then user has 50% probability to pass
 - if the answers provided by other users before were correct it does not mean that the probability is equal 100%
 - if there are a lot of wrong answers provided by other users the probability goes to 0%
 - if there are a lot of wrong answers provided by other users the probability goes to 100%
 - if user provided wrong answer it means that the probability is equal to 0%
 - if user provided all correct answers it means that the probability is equal to 100%

## How to deploy
1. docker-compose up -d

## How to rebuild the app
1. docker-compose up -d --build

## How to create and seed db
1. docker-compose exec webapp python manage.py recreate_db
2. docker-compose exec webapp python manage.py seed_db

## How to test application
1. docker-compose exec webapp python manage.py test

## How to use the app
1. Go to http://127.0.0.1:5000/api/v1/docs
2. Register a user (POST /users)
3. Login (POST /auth/login) and get the access token
4. Insert the access token with the "Bearer" prefix
5. Get surveys (GET /surveys)
6. Join survey (POST/surveys/{survey_id})
7. Get the survey's questions (GET /surveys/{survey_id}/questions)
8. Get the question with your probability to be a panelist (GET /questions/{question_id})
9. Provide the answer  using (POST /questions/{question_id})
10. Close the survey (DELETE /surveys/{survey_id})