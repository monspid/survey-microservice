#
# !!!DO NOT READ THIS!!!
# THIS FILE JUST LOADS INITIAL DATA
#


import csv
import os
from collections import OrderedDict

from webapp import db
from webapp.api.models.db_models import Question, Survey, User, UserSurvey, UserQuestion

fist_survey_questions = OrderedDict([
    (
        "non resident", {
            "content": "Do you live outside of UK?",
            "answer": False
        }
    ),
    (
        "underaged", {
            "content": "Are you underaged?",
            "answer": False
        }
    )
    ,
    (
        "unemployed", {
            "content": "Are you unemployed?",
            "answer": False
        }
    ),
    (
        "imprisoned", {
            "content": "Have you ever been in prison?",
            "answer": False
        }
    ),
    (
        "previous_survey", {
            "content": "Have you previously participated in an online survey?",
            "answer": False
        }
    )
])


def seed_first_survey_csv(limit=None):
    dir_path = os.path.dirname(os.path.realpath(__file__))

    with open(os.path.join(dir_path, 'first_survey.csv')) as csvfile:
        survey_reader = csv.reader(csvfile)

        questions_names = get_questions_names(survey_reader)

        if questions_names:
            fill_db(questions_names, survey_reader, limit)

        db.session.commit()


def fill_db(questions_names, survey_reader, limit):
    survey, questions = add_survey()

    for username, answers in enumerate(survey_reader):
        if limit == 0:
            break

        if is_correct_row(answers, questions_names):
            add_user_answer(answers, questions, survey, username)
            db.session.flush()

            if limit is not None:
                limit -= 1


def add_user_answer(answers, questions, survey, username):
    user = add_user(username)
    user_survey = add_user_survey(survey, user)

    is_panelist = add_answer(answers, questions, user)

    user_survey.is_panelist = is_panelist
    db.session.add(user)


def add_answer(answers, questions, user):
    is_panelist = True

    for it, answer in enumerate(answers):
        answer = convert_answer(answer)

        user_question = UserQuestion(answer=answer)
        user_question.question = questions[it]
        user.questions.append(user_question)

        is_panelist = is_panelist and (questions[it].answer == answer)
    return is_panelist


def add_user_survey(survey, user):
    user_survey = UserSurvey()
    user_survey.survey = survey
    user.surveys.append(user_survey)
    return user_survey


def add_user(username):
    user = User(name=str(username), password=str(username))
    return user


def add_survey():
    survey = Survey(name='first_survey')
    questions = add_questions(survey)
    db.session.add(survey)
    return survey, questions


def is_correct_row(row, questions_names):
    return len(row) == len(questions_names)


def convert_answer(answer):
    if answer == 'T':
        return True
    elif answer == 'F':
        return False
    else:
        return None


def get_questions_names(survey_reader):
    try:
        question_names = next(survey_reader)
        correct_question_names = list(fist_survey_questions.keys())

        if not are_lists_the_same(question_names, correct_question_names):
            return None

        return question_names

    except StopIteration:
        return None


def are_lists_the_same(list_, other_list):
    if len(list_) != len(other_list):
        return False

    for it, item in enumerate(list_):
        if item != other_list[it]:
            return False

    return True


def add_questions(survey):
    it = 1

    questions = list()

    for fist_survey_key, fist_survey_value in fist_survey_questions.items():
        question = Question(content=fist_survey_value['content'], order=it, answer=fist_survey_value['answer'])
        questions.append(question)

        it += 1

    survey.questions.extend(questions)

    return questions
