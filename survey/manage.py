import sys
import unittest

from flask.cli import FlaskGroup

from utils.db.first_survey_csv_seeder import seed_first_survey_csv
from webapp import create_app, db

app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()


@cli.command('seed_db')
def seed_db():
    seed_first_survey_csv()


@cli.command('test')
def test():
    tests = unittest.TestLoader().discover('webapp/tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)

    if result.wasSuccessful():
        return 0

    sys.exit(result)


if __name__ == '__main__':
    cli()
