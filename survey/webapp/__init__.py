import os

from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_sqlalchemy import SQLAlchemy

from webapp.config import configs

db = SQLAlchemy()
cors = CORS()
jwt = JWTManager()


def create_app():
    app = Flask(__name__)

    app_settings = os.getenv('APP_SETTINGS')
    app.config.from_object(configs[app_settings])

    db.init_app(app)
    cors.init_app(app)
    jwt.init_app(app)

    from webapp.api import api_blueprint
    app.register_blueprint(api_blueprint)

    @app.shell_context_processor
    def ctx():
        return {'app': app, 'db': db}

    return app
