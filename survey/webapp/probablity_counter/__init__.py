from webapp.probablity_counter.probability_answer_factory import ProbabilityAnswerFactory


def calculate_probability(survey_questions, user_id):
    probability = 0
    probability_answers = ProbabilityAnswerFactory()

    for survey_question in survey_questions:
        user_answered_wrong = process_question_answers(survey_question, user_id, probability_answers)

        if user_answered_wrong:
            break
    else:
        probability = probability_answers.get_probability()

    return round(probability, 2)


def process_question_answers(survey_question, user_id, probability_answers):
    user_answered_wrong = False
    probability_answer = probability_answers.create()

    for user_answer in survey_question.users:
        if user_answered_question(user_answer):
            if user_answer.user_id == user_id:
                if survey_question.answer != user_answer.answer:
                    user_answered_wrong = True
                else:
                    probability_answers.delete(-1)

                break
            else:
                add_answer_value_to_factory(probability_answer, survey_question, user_answer)

    return user_answered_wrong


def user_answered_question(user_answer):
    return user_answer.answer is not None


def add_answer_value_to_factory(probability_answer, survey_question, user_answer):
    if survey_question.answer == user_answer.answer:
        probability_answer.correct += 1
    else:
        probability_answer.wrong += 1
