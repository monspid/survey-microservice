class ProbabilityAnswer:
    def __init__(self):
        self.correct = 0
        self.wrong = 0

    def get_probability(self):
        all_answers = self.correct + self.wrong
        return (2 * self.correct + 1) / (all_answers + 1) / 2


class ProbabilityAnswerFactory:
    def __init__(self):
        self.probability_answers = list()

    def create(self):
        probability_answer = ProbabilityAnswer()
        self.probability_answers.append(probability_answer)

        return probability_answer

    def delete(self, index):
        del self.probability_answers[index]

    def get_probability(self):
        probability = 1

        for probability_answer in self.probability_answers:
            probability *= probability_answer.get_probability()

        return probability
