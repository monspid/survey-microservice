from http import HTTPStatus

from flask_jwt_extended import create_access_token
from flask_restplus import abort
from sqlalchemy.sql.elements import and_

from webapp.api.models.db_models import User


def login(username, password):
    user = User.query.filter(and_(User.name == username)).first()

    if not user or not user.check_password(password):
        abort(HTTPStatus.UNAUTHORIZED, 'Wrong Username or password')

    access_token = create_access_token(identity=user.id)
    return access_token
