from http import HTTPStatus

from flask_restplus import abort
from sqlalchemy import or_
from sqlalchemy.exc import IntegrityError

from webapp import db
from webapp.api.controlers import rollback_handler, get_user_survey
from webapp.api.models.db_models import Survey, User, UserSurvey, Question, UserQuestion


def get_surveys():
    return Survey.query.all()


def get_survey(survey_id, user_id, with_for_update=False):
    survey = get_user_survey(survey_id, user_id, with_for_update)

    if not survey:
        abort(HTTPStatus.NOT_FOUND, 'Survey does not exists or you have not joined it')

    return survey


def get_survey_questions(survey_id, user_id):
    survey = get_survey(survey_id, user_id)

    return Question.query.filter(Question.survey_id == survey.id).all()


@rollback_handler
def join_survey(survey_id, user_id):
    user = User.query.filter(User.id == user_id).first()
    survey = Survey.query.filter(Survey.id == survey_id).first()

    if not survey:
        abort(HTTPStatus.NOT_FOUND, 'Survey does not exists')

    try:
        with db.session.no_autoflush:
            create_user_survey_relation(user, survey)
            create_user_questions_relations(user, survey.questions)
            db.session.add(user)

        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        abort(HTTPStatus.CONFLICT, 'UserSurvey relation already exists')


def create_user_survey_relation(user, survey):
    user_survey = UserSurvey(user_id=user.id, survey_id=survey.id)
    db.session.add(user_survey)


def create_user_questions_relations(user, questions):
    for question in questions:
        user_question = UserQuestion(user_id=user.id, question_id=question.id)
        db.session.add(user_question)


@rollback_handler
def close_survey(survey_id, user_id):
    survey = get_survey(survey_id, user_id, with_for_update=True)
    survey_user = survey.users[0]

    if survey_user.is_panelist is not None:
        abort(HTTPStatus.CONFLICT, 'Survey is already closed')

    survey_user.is_panelist = is_panelist(survey_id, user_id)
    db.session.add(survey_user)
    db.session.commit()

    return survey_user.is_panelist


def is_panelist(survey_id, user_id):
    wrong_answers = Question.query \
        .join(UserQuestion) \
        .filter(Question.survey_id == survey_id,
                UserQuestion.user_id == user_id,
                or_(Question.answer != UserQuestion.answer, UserQuestion.answer.is_(None))
                ) \
        .all()

    return not wrong_answers
