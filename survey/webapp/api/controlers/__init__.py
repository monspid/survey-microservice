from sqlalchemy.orm import contains_eager

from webapp import db
from webapp.api.models.db_models import Survey, UserSurvey, User


def rollback_handler(func):
    def func_wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            db.session.rollback()
            raise e

    return func_wrapper


def get_user_survey(survey_id, user_id, with_for_update=False):
    query = Survey.query \
        .join(UserSurvey) \
        .options(contains_eager(Survey.users)) \
        .filter(Survey.id == survey_id, UserSurvey.user_id == user_id)

    if with_for_update:
        query = query.with_for_update()

    return query.first()
