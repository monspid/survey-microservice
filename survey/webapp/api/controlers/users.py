from http import HTTPStatus

from flask_restplus import abort
from sqlalchemy.exc import IntegrityError

from webapp import db
from webapp.api.controlers import rollback_handler
from webapp.api.models.db_models import User


@rollback_handler
def register(username, password):
    user = User(name=username, password=password)
    db.session.add(user)

    try:
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        abort(HTTPStatus.CONFLICT, 'User already exists')
