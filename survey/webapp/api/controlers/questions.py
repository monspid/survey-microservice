from http import HTTPStatus

from flask_restplus import abort
from sqlalchemy.orm import contains_eager

from webapp import db
from webapp.api.controlers import get_user_survey, rollback_handler
from webapp.api.models.db_models import Question, UserQuestion
from webapp.probablity_counter import calculate_probability


def get_user_question(question_id, user_id):
    question = Question.query \
        .join(UserQuestion) \
        .options(contains_eager(Question.users)) \
        .filter(Question.id == question_id, UserQuestion.user_id == user_id) \
        .first()

    if not question:
        abort(HTTPStatus.NOT_FOUND, "Question does not exists")

    return question


def get_question(question_id, user_id):
    question = get_user_question(question_id, user_id)

    survey_questions = Question.query \
        .join(UserQuestion) \
        .filter(Question.survey_id == question.survey_id) \
        .all()

    probability = calculate_probability(survey_questions, user_id)

    question.probability = probability

    return question


@rollback_handler
def answer_question(answer, question_id, user_id):
    question = get_user_question(question_id, user_id)

    user_question = question.users[0]

    if user_question.answer is not None:
        abort(HTTPStatus.CONFLICT, "You have answered for this question")

    survey_user = get_user_survey(question.survey_id, user_id, with_for_update=True)

    if survey_user.users[0].is_panelist is not None:
        abort(HTTPStatus.CONFLICT, "Survey is already closed")

    user_question.answer = answer

    db.session.add(user_question)
    db.session.commit()
