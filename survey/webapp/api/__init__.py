from flask import Blueprint
from flask_jwt_extended.exceptions import JWTExtendedException
from flask_restplus import Api
from jwt import InvalidTokenError

from webapp.api.views.auth import auth_api
from webapp.api.views.questions import questions_api
from webapp.api.views.surveys import surveys_api
from webapp.api.views.users import users_api

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    }
}

api_blueprint = Blueprint('api', __name__, url_prefix='/api/v1')
api = Api(api_blueprint, doc='/docs', authorizations=authorizations)

api.add_namespace(auth_api)
api.add_namespace(questions_api)
api.add_namespace(surveys_api)
api.add_namespace(users_api)


@api.errorhandler(JWTExtendedException)
def handle_jwt_exceptions(error):
    pass


@api.errorhandler(InvalidTokenError)
def handle_jwt_exceptions(error):
    pass
