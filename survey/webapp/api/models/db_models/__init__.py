from werkzeug.security import generate_password_hash, check_password_hash

from webapp import db


class Survey(db.Model):
    __tablename__ = 'survey'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(256), nullable=False, unique=True)
    questions = db.relationship('Question', back_populates='survey')
    users = db.relationship('UserSurvey', back_populates='survey')


class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(256), nullable=False, unique=True)
    _password = db.Column('password', db.String(256), nullable=False)
    questions = db.relationship('UserQuestion', back_populates='user')
    surveys = db.relationship('UserSurvey', back_populates='user')

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self._password, password)


class UserSurvey(db.Model):
    __tablename__ = 'user_survey'
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'), primary_key=True)
    is_panelist = db.Column(db.Boolean)
    user = db.relationship('User', back_populates='surveys')
    survey = db.relationship('Survey', back_populates='users')


class Question(db.Model):
    __tablename__ = 'question'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    content = db.Column(db.String(1024), nullable=False)
    order = db.Column(db.Integer, nullable=False)
    answer = db.Column(db.Boolean, nullable=False)
    survey_id = db.Column(db.Integer, db.ForeignKey('survey.id'))
    survey = db.relationship('Survey', back_populates='questions')
    users = db.relationship('UserQuestion', back_populates='question')


class UserQuestion(db.Model):
    __tablename__ = 'user_question'
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'), primary_key=True)
    answer = db.Column(db.Boolean)
    user = db.relationship('User', back_populates='questions')
    question = db.relationship('Question', back_populates='users')
