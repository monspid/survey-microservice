from flask_restplus import fields

question_model = {
    'id': fields.Integer(required=True, description='id'),
    'content': fields.String(required=True, description='The question content'),
    'answer': fields.Boolean(required=True, description='Users answer'),
    'probability': fields.Float(required=True, description='Panelist probability'),
}

answer_model = {
    'answer': fields.Boolean(required=True)
}
