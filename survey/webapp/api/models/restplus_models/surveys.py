from flask_restplus import fields

survey_list_model = {
    'id': fields.Integer(required=True, description='id'),
    'name': fields.String(required=True, description='The survey name')
}

survey_model = {
    'id': fields.Integer(required=True, description='id'),
    'name': fields.String(required=True, description='The survey name')
}

is_panelist_model = {
    'is_panelist': fields.Boolean(required=True, description='Is the panelist'),
}

survey_question_model = {
    'id': fields.Integer(required=True, description='id'),
    'content': fields.String(required=True, description='The question content'),
}
