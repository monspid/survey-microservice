from flask_restplus import fields

register_model = {
    'username': fields.String(required=True, min_length=1, max_length=256),
    'password': fields.String(required=True, min_length=1, max_length=256)
}
