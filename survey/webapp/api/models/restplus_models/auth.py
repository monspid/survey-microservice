from flask_restplus import fields

login_model = {
    'username': fields.String(required=True, min_length=1, max_length=256),
    'password': fields.String(required=True, min_length=1, max_length=256)
}

JWT_model = {
    'access_token': fields.String(required=True)
}
