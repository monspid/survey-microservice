from flask import request
from flask_restplus import Namespace, Resource

from webapp.api.controlers.auth import login
from webapp.api.models.restplus_models.auth import JWT_model, login_model

auth_api = Namespace('auth', description='Auth related tasks.')


@auth_api.route('/login')
class Auth(Resource):
    @auth_api.expect(auth_api.model('Login', login_model))
    @auth_api.marshal_with(auth_api.model('JWT', JWT_model))
    def post(self):
        """Login"""

        username = request.json.get('username')
        password = request.json.get('password')

        access_token = login(username, password)
        return dict(access_token=access_token)
