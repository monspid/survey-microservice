from flask import request
from flask_restplus import Namespace, Resource

from webapp.api.controlers.users import register
from webapp.api.models.restplus_models.user import register_model

users_api = Namespace('users', description='User related tasks.')


@users_api.route('')
class User(Resource):
    @users_api.expect(users_api.model('Register', register_model))
    def post(self):
        """Register"""

        username = request.json.get('username')
        password = request.json.get('password')

        register(username, password)
        return 'User has been registered'
