from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restplus import Namespace, Resource

from webapp.api.controlers.surveys import get_surveys, get_survey, get_survey_questions, join_survey, close_survey
from webapp.api.models.restplus_models.surveys import survey_question_model, survey_list_model, survey_model

surveys_api = Namespace('surveys', description='Surveys related tasks.')


@surveys_api.doc(security='apikey')
@surveys_api.route('')
class Survey(Resource):
    @jwt_required
    @surveys_api.marshal_with(surveys_api.model('SurveyList', survey_list_model))
    def get(self):
        """Get All Surveys"""

        surveys = get_surveys()

        return [dict(id=survey.id, name=survey.name) for survey in surveys]


@surveys_api.doc(security='apikey')
@surveys_api.route('/<int:survey_id>', doc=dict(params={'survey_id': 'Survey id'}))
class SurveyExisting(Resource):
    @jwt_required
    @surveys_api.marshal_list_with(surveys_api.model('Survey', survey_model))
    def get(self, survey_id):
        """Get the Survey"""

        user_id = get_jwt_identity()
        survey = get_survey(survey_id, user_id)

        return dict(id=survey.id, name=survey.name)

    @jwt_required
    def post(self, survey_id):
        """Join the Survey"""

        user_id = get_jwt_identity()
        join_survey(survey_id, user_id)

        return "User has joined the survey"

    @jwt_required
    @surveys_api.marshal_list_with(surveys_api.model('IsPanelist', survey_model))
    def delete(self, survey_id):
        """Close the Survey"""

        user_id = get_jwt_identity()
        is_panelist = close_survey(survey_id, user_id)

        return dict(is_panelist=is_panelist)


@surveys_api.doc(security='apikey')
@surveys_api.route('/<int:survey_id>/questions', doc=dict(params={'survey_id': 'Survey id'}))
class SurveyQuestions(Resource):
    @jwt_required
    @surveys_api.marshal_list_with(surveys_api.model('SurveyQuestion', survey_question_model))
    def get(self, survey_id):
        """Get Questions related with the Survey"""

        user_id = get_jwt_identity()
        questions = get_survey_questions(survey_id, user_id)

        return [dict(id=question.id, content=question.content) for question in questions]
