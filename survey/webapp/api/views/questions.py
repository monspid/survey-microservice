from flask import request
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restplus import Namespace, Resource

from webapp.api.controlers.questions import get_question, answer_question
from webapp.api.models.restplus_models.questions import question_model, answer_model

questions_api = Namespace('questions', description='Questions related tasks.')


@questions_api.doc(security='apikey')
@questions_api.route('/<int:question_id>', doc=dict(params={'question_id': 'Question id'}))
class Question(Resource):
    @jwt_required
    @questions_api.marshal_with(questions_api.model('Question', question_model))
    def get(self, question_id):
        """Get Question related with User"""

        user_id = get_jwt_identity()

        question = get_question(question_id, user_id)

        return dict(id=question.id, content=question.content, answer=question.answer, probability=question.probability)

    @jwt_required
    @questions_api.expect(questions_api.model('Answer', answer_model))
    def post(self, question_id):
        """Provide answer for the question"""

        user_id = get_jwt_identity()
        answer = request.json.get('answer')

        answer_question(answer, question_id, user_id)

        return 'User answered a question'
