import json
from http import HTTPStatus

from webapp.api.models.db_models import UserQuestion
from webapp.tests.base import BaseTestCase


class TestQuestion(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.url_prefix_ex = self.url_prefix + '/questions'
        self.token = self.create_user_and_get_access_token()

    def create_and_join_survey(self):
        survey = self.create_surveys_with_questions(question_range=10)

        join_survey_url = self.url_prefix + f'/surveys/{survey.id}'
        self.client.post(join_survey_url, content_type='application/json', headers=dict(Authorization=self.token))

        return survey

    def test_answer_question(self):
        with self.client:
            survey = self.create_and_join_survey()
            question = survey.questions[0]

            url = f"{self.url_prefix_ex}/{question.id}"
            data = dict(answer=True)

            ans_res = self.client.post(url, data=json.dumps(data), content_type='application/json',
                                       headers=dict(Authorization=self.token))

            user_question = UserQuestion.query.filter(UserQuestion.question_id == question.id).first()

            self.assertEqual(user_question.answer, data['answer'])

            self.assert200(ans_res)

    def test_answer_question_answer_does_not_exist(self):
        with self.client:
            question_id_does_not_exist = -1

            url = f"{self.url_prefix_ex}/{question_id_does_not_exist}"
            data = dict(answer=True)

            ans_res = self.client.post(url, data=json.dumps(data), content_type='application/json',
                                       headers=dict(Authorization=self.token))

            self.assert404(ans_res)

    def test_answer_question_already_answered(self):
        with self.client:
            survey = self.create_and_join_survey()
            question = survey.questions[0]

            url = f"{self.url_prefix_ex}/{question.id}"
            data = dict(answer=True)

            ans_res1 = self.client.post(url, data=json.dumps(data), content_type='application/json',
                                        headers=dict(Authorization=self.token))

            ans_res2 = self.client.post(url, data=json.dumps(data), content_type='application/json',
                                        headers=dict(Authorization=self.token))

            user_question = UserQuestion.query.filter(UserQuestion.question_id == question.id).first()

            self.assertEqual(user_question.answer, data['answer'])

            self.assert200(ans_res1)
            self.assertEqual(ans_res2.status_code, HTTPStatus.CONFLICT)

    def test_answer_question_survey_closed(self):
        with self.client:
            survey = self.create_and_join_survey()
            question = survey.questions[0]

            close_url = f"{self.url_prefix}/surveys/{survey.id}"
            close_res = self.client.delete(close_url, content_type='application/json',
                                           headers=dict(Authorization=self.token))

            answer_url = f"{self.url_prefix_ex}/{question.id}"
            data = dict(answer=True)

            ans_res = self.client.post(answer_url, data=json.dumps(data), content_type='application/json',
                                       headers=dict(Authorization=self.token))

            self.assertEqual(ans_res.status_code, HTTPStatus.CONFLICT)

    def test_answer_question_without_jwt(self):
        with self.client:
            url = f"{self.url_prefix_ex}/1"
            data = dict(answer=True)

            answer_res = self.client.post(url, data=json.dumps(data), content_type='application/json')

            self.assert401(answer_res)

    def get_question(self):
        with self.client:
            survey = self.create_and_join_survey()
            question = survey.questions[0]

            url = f"{self.url_prefix_ex}/{question.id}"

            ans_res = self.client.get(url, content_type='application/json',
                                      headers=dict(Authorization=self.token))

            self.assertAlmostEqual(ans_res.json.get('probability'), 0.5)

            url = f"{self.url_prefix_ex}/{question.id}"
            data = dict(answer=True)

            wrong_answer = self.client.post(url, data=json.dumps(data), content_type='application/json',
                                            headers=dict(Authorization=self.token))

            ans_res = self.client.get(url, content_type='application/json',
                                      headers=dict(Authorization=self.token))

            self.assertAlmostEqual(ans_res.json.get('probability'), 0)

            self.assert200(ans_res)

    def get_question_question_does_not_exists(self):
        with self.client:
            question_does_not_exist = -1
            url = f"{self.url_prefix_ex}/{question_does_not_exist}"

            data = dict(answer=True)

            ans_res = self.client.post(url, data=json.dumps(data), content_type='application/json',
                                       headers=dict(Authorization=self.token))
            self.assert404(ans_res)

    def get_question_question_without_jwt(self):
        with self.client:
            url = f"{self.url_prefix_ex}/1"

            answer_res = self.client.get(url, content_type='application/json')

            self.assert401(answer_res)
