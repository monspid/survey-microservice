import json
from http import HTTPStatus

from webapp.api.models.db_models import User
from webapp.tests.base import BaseTestCase


class TestUser(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.url_prefix_ex = self.url_prefix + '/users'

    def test_register(self):
        with self.client:
            data = dict(username='username', password='password')
            user_created = self.client.post(self.url_prefix_ex, data=json.dumps(data), content_type='application/json')

            user = User.query.filter(User.name == data['username']).first()

            self.assertIsNotNone(user)
            self.assertEqual(user_created.status_code, HTTPStatus.OK)

    def test_register_user_already_exists(self):
        with self.client:
            data = dict(username='username', password='password')
            user_created = self.client.post(self.url_prefix_ex, data=json.dumps(data), content_type='application/json')
            user_recreated = self.client.post(self.url_prefix_ex, data=json.dumps(data),
                                              content_type='application/json')

            self.assertEqual(user_created.status_code, HTTPStatus.OK)
            self.assertEqual(user_recreated.status_code, HTTPStatus.CONFLICT)
