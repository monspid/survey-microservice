from webapp.tests.base import BaseTestCase


class TestAuth(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.url_prefix_ex = self.url_prefix + '/auth'
        self.create_user()

    def test_auth(self):
        with self.client:
            user_logged_in = self.login_user(self.user_data)

            self.assert200(user_logged_in)
            self.assertIsNotNone(user_logged_in.json.get('access_token'))

    def test_auth_invalid_token(self):
        """
        In following tests should be checked all invalid tokens
        """
        pass
