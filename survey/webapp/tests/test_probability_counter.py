from webapp.probablity_counter.probability_answer_factory import ProbabilityAnswer, ProbabilityAnswerFactory
from webapp.tests.base import BaseTestCase


class TestProbabilityCounter(BaseTestCase):
    def test_probability_answer_no_answer_provided(self):
        probability_answer = ProbabilityAnswer()

        self.assertAlmostEqual(probability_answer.get_probability(), 0.5)

    def test_probability_answer_not_many_correct_answers(self):
        probability_answer = ProbabilityAnswer()
        probability_answer.correct = 2

        self.assertNotAlmostEqual(probability_answer.get_probability(), 1.0, delta=0.1)

    def test_probability_answer_many_correct_answers(self):
        probability_answer = ProbabilityAnswer()
        probability_answer.correct = 1000000

        self.assertAlmostEqual(probability_answer.get_probability(), 1.0, delta=0.1)

    def test_probability_answer_many_wrong_answers(self):
        probability_answer = ProbabilityAnswer()
        probability_answer.wrong = 1000000

        self.assertAlmostEqual(probability_answer.get_probability(), 0.0, delta=0.1)

    def test_probability_answer_factory_all_correct_answers_provided(self):
        probability_answer_factory = ProbabilityAnswerFactory()

        self.assertEqual(probability_answer_factory.get_probability(), 1.0)

    def test_probability_answer_factory_no_answer_provided(self):
        probability_answer_factory = ProbabilityAnswerFactory()

        n_elements = 3

        for i in range(n_elements):
            probability_answer_factory.create()

        self.assertAlmostEqual(probability_answer_factory.get_probability(), 0.5 ** n_elements)

    def test_probability_answer_factory_many_correct_answers(self):
        probability_answer_factory = ProbabilityAnswerFactory()

        n_elements = 3

        for i in range(n_elements):
            probability_answer = probability_answer_factory.create()
            probability_answer.correct = 1000000

        self.assertAlmostEqual(probability_answer_factory.get_probability(), 1, delta=0.1)

    def test_probability_answer_factory_not_many_correct_answers(self):
        probability_answer_factory = ProbabilityAnswerFactory()

        n_elements = 3

        for i in range(n_elements):
            probability_answer = probability_answer_factory.create()
            probability_answer.correct = 2

        self.assertNotAlmostEqual(probability_answer_factory.get_probability(), 1, delta=0.1)

    def test_probability_answer_factory_many_wrong_answers(self):
        probability_answer_factory = ProbabilityAnswerFactory()

        n_elements = 3

        for i in range(n_elements):
            probability_answer = probability_answer_factory.create()
            probability_answer.correct = 1000000

        self.assertAlmostEqual(probability_answer_factory.get_probability(), 1, delta=0.1)
