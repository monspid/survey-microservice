from http import HTTPStatus

from webapp.api.models.db_models import UserSurvey
from webapp.tests.base import BaseTestCase


class TestSurvey(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.url_prefix_ex = self.url_prefix + '/surveys'
        self.token = self.create_user_and_get_access_token()

    def test_join_survey(self):
        with self.client:
            survey = self.create_surveys_with_questions()
            url = f'{self.url_prefix_ex}/{survey.id}'

            join_res = self.client.post(url, content_type='application/json', headers=dict(Authorization=self.token))
            user_surveys = UserSurvey.query.filter(UserSurvey.survey_id == survey.id).all()

            self.assertEqual(len(user_surveys), 1)
            self.assertEqual(user_surveys[0].user.name, self.username)
            self.assert200(join_res)

    def test_join_survey_survey_does_not_exist(self):
        with self.client:
            survey_does_not_exist_id = 10
            url = f'{self.url_prefix_ex}/{survey_does_not_exist_id}'

            join_res = self.client.post(url, content_type='application/json', headers=dict(Authorization=self.token))

            self.assert404(join_res)

    def test_join_survey_survey_relation_already_exists(self):
        with self.client:
            survey = self.create_surveys_with_questions()
            url = f'{self.url_prefix_ex}/{survey.id}'

            first_join = self.client.post(url, content_type='application/json', headers=dict(Authorization=self.token))
            second_join = self.client.post(url, content_type='application/json', headers=dict(Authorization=self.token))

            user_surveys = UserSurvey.query.filter(UserSurvey.survey_id == survey.id).all()

            self.assertEqual(len(user_surveys), 1)
            self.assertEqual(user_surveys[0].user.name, self.username)
            self.assert200(first_join)
            self.assertEqual(second_join.status_code, HTTPStatus.CONFLICT)

    def test_join_survey_without_jwt(self):
        with self.client:
            survey = self.create_surveys_with_questions()
            url = f'{self.url_prefix_ex}/{survey.id}'
            join_res = self.client.post(url, content_type='application/json')

            self.assert401(join_res)

    def test_close_survey(self):
        with self.client:
            survey = self.create_surveys_with_questions()
            url = f'{self.url_prefix_ex}/{survey.id}'

            join_res = self.client.post(url, content_type='application/json', headers=dict(Authorization=self.token))
            close_res = self.client.delete(url, content_type='application/json', headers=dict(Authorization=self.token))

            user_surveys = UserSurvey.query.filter(UserSurvey.survey_id == survey.id).all()

            self.assertFalse(close_res.json.get('is_panelist'))
            self.assertIsNotNone(len(user_surveys), 1)
            self.assertEqual(user_surveys[0].user.name, self.username)
            self.assertIsNotNone(user_surveys[0].is_panelist)
            self.assert200(close_res)

    def test_close_survey_survey_does_not_exist(self):
        with self.client:
            survey_does_not_exist_id = 10
            url = f'{self.url_prefix_ex}/{survey_does_not_exist_id}'

            close_res = self.client.delete(url, content_type='application/json', headers=dict(Authorization=self.token))

            self.assert404(close_res)

    def test_close_survey_survey_relation_already_exists(self):
        with self.client:
            survey = self.create_surveys_with_questions()
            url = f'{self.url_prefix_ex}/{survey.id}'

            join_res = self.client.post(url, content_type='application/json', headers=dict(Authorization=self.token))
            close_1 = self.client.delete(url, content_type='application/json', headers=dict(Authorization=self.token))
            close_2 = self.client.post(url, content_type='application/json', headers=dict(Authorization=self.token))

            user_surveys = UserSurvey.query.filter(UserSurvey.survey_id == survey.id).all()

            self.assertEqual(len(user_surveys), 1)
            self.assertEqual(user_surveys[0].user.name, self.username)
            self.assert200(close_1)
            self.assertEqual(close_2.status_code, HTTPStatus.CONFLICT)

    def test_close_survey_without_jwt(self):
        with self.client:
            survey = self.create_surveys_with_questions()
            url = f'{self.url_prefix_ex}/{survey.id}'
            join_res = self.client.delete(url, content_type='application/json')

            self.assert401(join_res)

    def test_get_surveys(self):
        with self.client:
            surveys = self.create_surveys_with_questions(survey_range=5)
            get_surveys_res = self.client.get(self.url_prefix_ex, content_type='application/json',
                                              headers=dict(Authorization=self.token))

            surveys_ids = [survey.get('id') for survey in get_surveys_res.json]
            self.assertTrue(all([survey.id in surveys_ids for survey in surveys]))

            self.assert200(get_surveys_res)

    def test_get_surveys_without_jwt(self):
        with self.client:
            get_surveys_res = self.client.get(self.url_prefix_ex, content_type='application/json')
            self.assert401(get_surveys_res)

    def test_get_survey(self):
        with self.client:
            surveys = self.create_surveys_with_questions(survey_range=5)
            survey = surveys[0]

            join_survey_url = f'{self.url_prefix_ex}/{survey.id}'
            self.client.post(join_survey_url, content_type='application/json', headers=dict(Authorization=self.token))

            join_survey_url = self.url_prefix_ex + f'/{survey.id}'
            get_surveys_res = self.client.get(join_survey_url, content_type='application/json',
                                              headers=dict(Authorization=self.token))

            self.assertEqual(get_surveys_res.json.get('id'), survey.id)

            self.assert200(get_surveys_res)

    def test_get_survey_survey_does_not_exist(self):
        with self.client:
            survey_does_not_exist_id = 10

            url = self.url_prefix_ex + f'/{survey_does_not_exist_id}'
            get_surveys_res = self.client.get(url, content_type='application/json',
                                              headers=dict(Authorization=self.token))

            self.assert404(get_surveys_res)

    def test_get_survey_survey_user_did_not_join_it(self):
        with self.client:
            survey = self.create_surveys_with_questions()

            url = self.url_prefix_ex + f'/{survey.id}'
            get_surveys_res = self.client.get(url, content_type='application/json',
                                              headers=dict(Authorization=self.token))

            self.assert404(get_surveys_res)

    def test_get_survey_without_jwt(self):
        with self.client:
            url = self.url_prefix_ex + '/1'
            get_surveys_res = self.client.get(url, content_type='application/json')

            self.assert401(get_surveys_res)

    def test_get_survey_questions(self):
        with self.client:
            survey = self.create_surveys_with_questions(question_range=10)
            url = self.url_prefix_ex + f'/{survey.id}/questions'

            join_survey_url = f'{self.url_prefix_ex}/{survey.id}'
            self.client.post(join_survey_url, content_type='application/json', headers=dict(Authorization=self.token))

            get_surveys_res = self.client.get(url, content_type='application/json',
                                              headers=dict(Authorization=self.token))

            questions_id = [question.get('id') for question in get_surveys_res.json]
            self.assertTrue(all([question.id in questions_id for question in survey.questions]))

            self.assert200(get_surveys_res)

    def test_get_survey_questions_survey_does_not_exist(self):
        with self.client:
            survey_does_not_exist_id = 10

            url = self.url_prefix_ex + f'/{survey_does_not_exist_id}/questions'
            get_surveys_res = self.client.get(url, content_type='application/json',
                                              headers=dict(Authorization=self.token))

            self.assert404(get_surveys_res)

    def test_get_survey_questions_without_jwt(self):
        with self.client:
            url = self.url_prefix_ex + '/1/questions'
            get_surveys_res = self.client.get(url, content_type='application/json')

            self.assert401(get_surveys_res)
