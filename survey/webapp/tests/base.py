from flask_testing import TestCase
import json

from utils.db.first_survey_csv_seeder import seed_first_survey_csv
from webapp import create_app, db, configs
from webapp.api.models.db_models import Survey, Question

app = create_app()


class BaseTestCase(TestCase):

    def create_app(self):
        app.config.from_object(configs['testing'])
        return app

    def setUp(self):
        self.url_prefix = '/api/v1'

        db.drop_all()
        db.create_all()
        seed_first_survey_csv(limit=1)

        db.session.commit()

    def tearDown(self):
        db.session.remove()

    def create_user_and_get_access_token(self):
        user_data = self.create_user()
        user_logged_in = self.login_user(user_data)
        return f'Bearer {user_logged_in.json.get("access_token")}'

    def create_user(self):
        self.username = 'username'
        self.user_password = 'password'
        self.user_data = dict(username=self.username, password=self.user_password)
        self.client.post(self.url_prefix + '/users', data=json.dumps(self.user_data), content_type='application/json')

        return self.user_data

    def login_user(self, user_data):
        url = self.url_prefix + '/auth/login'
        return self.client.post(url, data=json.dumps(user_data), content_type='application/json')

    @staticmethod
    def create_surveys_with_questions(survey_range=1, question_range=1):
        surveys = list()
        for survey_number in range(survey_range):
            survey = Survey(name=f'survey{survey_number}')
            questions = [
                Question(content=str(question_number), order=question_number, answer=False) for question_number
                in range(question_range)
            ]

            survey.questions.extend(questions)
            surveys.append(survey)

        db.session.add_all(surveys)
        db.session.commit()

        if len(surveys) == 1:
            surveys = surveys[0]

        return surveys
