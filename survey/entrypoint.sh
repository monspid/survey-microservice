#!/usr/bin/env bash

echo "Waiting for DB..."

while ! nc -z db 5432; do
  sleep 1
done

echo "DB started"

gunicorn --workers=2 -b 0.0.0.0:5000 manage:app
